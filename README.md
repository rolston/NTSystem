# PowerShell Assume NT System
Run following to open up a powershell console under the NT System account.
```PowerShell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/rolston/NTSystem/raw/master/assume-ntsystem.ps1')
```
